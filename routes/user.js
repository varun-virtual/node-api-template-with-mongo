const express = require('express');
const { body} = require('express-validator');

const router = express.Router();

const userController = require('../controllers/user');
const isAuth = require('../middleware/auth');

router.post('/login', [
    body('email').isEmail()
        .withMessage('Please enter a valid email.')
        .normalizeEmail(),
    body('password').trim().isLength({ min: 6 }).withMessage('Please enter password minimum length.'),
], userController.login); //login user
 
router.get('/', isAuth , userController.getUsers); //fetch all user

router.get('/create', isAuth , userController.AddUser); //dependant data for creating user

router.post('/', isAuth , [
    body('username').trim().exists().withMessage('Please enter username'),
    body('first_name').trim().exists().withMessage('Please enter first name'),
    body('last_name').trim().exists().withMessage('Please enter first name'),
    body('email').isEmail().normalizeEmail().withMessage('Please enter valid email.'),
    body('password').trim().exists().withMessage('Please enter password'),
    body('role_id').trim().exists().withMessage('Please choose role'),
    body('status').trim().exists().withMessage('Please choose status'),
], userController.saveUser); //create a single user

router.get('/:userId', isAuth , userController.getUser); //fetch single user

router.put('/:userId', isAuth , [
    body('username').trim().exists().withMessage('Please enter username'),
    body('first_name').trim().exists().withMessage('Please enter first name'),
    body('last_name').trim().exists().withMessage('Please enter first name'),
    body('email').isEmail().normalizeEmail().withMessage('Please enter valid email.'),
    body('role_id').trim().exists().withMessage('Please choose role'),
    body('status').trim().exists().withMessage('Please choose status'),
], userController.updateUser); //fetch single user

router.delete('/:userId', isAuth , userController.deleteUser); //delete user

module.exports = router;