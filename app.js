const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');

const app = express();

const {dbUrl, port} = require('./utils/constants');

const dependantData = require('./routes/dependant');
const userRoutes = require('./routes/user');
const { handleErrorResponse } = require('./utils/response-helper');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/images/user');
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});

const fileFilter = (req,file,cb) => {
    if(file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg')
        cb(null, true);
    else
        cb(null, false);
}

app.use(bodyParser.urlencoded({extended : true })); // x-www-form-urlencoded <form>
app.use(bodyParser.json()); // application/json
app.use(multer({storage, limits: {fileSize: 200 * 1024 * 1024}, fileFilter}).single('image')); // for parsing multipart/form-data
app.use('/public/images/user', express.static(path.join(__dirname,'public/images/user')));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers','Content-Type, Authorization');
    next();
})

app.use('/api',dependantData);
app.use('/api/user',userRoutes);

app.use((error, req, res, next) => {
    handleErrorResponse(res, error);
})

mongoose.connect(dbUrl,{
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex : true
    })
    .then(() => {
        app.listen(port);
    }).catch(err => console.log(err));