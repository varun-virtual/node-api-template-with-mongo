# About:-
This project is a basic template of creating REST-API servervices based on NODEJS with Mongoose. It consists of basic features like CRUD operation, uploading of image, sending email to user along with customized pattern of error handling.

# Prequisites:-
Node

NPM

# Installation:-
After you clone this project, do the following:

### create a constants configuration file
cp  utils/constants-example.js utils/constants.js

### install npm dependencies
npm install

### add the config to your constants file
dbName=

port=

### add the mail connection configuration to your constants file for email to work
mailHost = '';

mailPort = ;

mailUsername = '';

mailPassword = '';

### create folder for image upload
create a folder named "user" inside having path as "public/images" for the image functionality to work smoothly.

### run webpack and watch for changes
npm start