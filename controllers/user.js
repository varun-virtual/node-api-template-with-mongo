const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');
const path = require('path');
const fs = require('fs');

const User = require('../models/user');
const Role = require('../models/role');
const { notFound, handleSuccessResponse, unauthorizedRequest, badRequest, validationError } = require('../utils/response-helper');
const { pagination, sendEmail, isObjectIdValid } = require('../utils/helper');
const { recordsPerPage, imageUrl } = require('../utils/constants');


/**method to check whether login user is valid or not */
exports.login = async (req, res, next) => {
    let loadedUser;
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors[0].msg)
        }

        const {email, password } = req.body;
        const user = await User.findOne({ email: email });
        if (!user) {
            throw notFound('A user with this email could not be found.');
        }
        loadedUser = user;
        const isEqual = await bcrypt.compare(password, user.password);
        if (!isEqual) {
            throw unauthorizedRequest('Wrong Password');
        }

        const token = jwt.sign(
            {
                email: loadedUser.email,
                userId: loadedUser._id.toString()
            },
            'somesupersecretsecret',
            { expiresIn: '1h' }
        );
        
        const data = {
            id : loadedUser._id.toString(),
            username: loadedUser.username,
            email: loadedUser.email,
            role_id: loadedUser.role_id,
            photo: loadedUser.photo,
            status: loadedUser.status,
            auth_token: token,
            full_name: loadedUser.first_name + ' ' + loadedUser.last_name
        }
        return handleSuccessResponse(res, data)

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method to fetch all record */
exports.getUsers = async (req, res, next) => {
    try {
        let {page, search, status, designation} = req.query;
        designation = designation ? ((designation)) : null;
        status = status ? ((status)) : null;
        const currentPage = parseInt(page) || 1;
        const perPage = recordsPerPage;
        let finalData = exportUserData = roleData = [];
        
        const query ={
            email: { $ne: 'instatedev@gmail.com' }
        }
        if (status) {
            query.status = status
        }
        if (designation) {
            query.role_id = designation
        }
        if (search) {
            //query.username = {'$regex' : search, '$options' : 'i'}
            //delete query.email
            query['$or'] = [
                { email: {'$regex' : search, '$options' : 'i'}},
                { username: {'$regex' : search, '$options' : 'i'}},
            ]
        }

        const totalItems = await User.find(query).countDocuments();
        const userData = await User.find(query)
                                    .sort({_id : -1})
                                    .populate('role_id')
                                    .skip((currentPage - 1) * perPage).limit(perPage);

        if(userData){
            const userDependantData = await Role.find({ name: { $ne: 'Administrator' } });
            finalData = userData.map(obj => {
                return {
                    "id"            : obj._id,
                    "username"      : obj.username,
                    "first_name"    : obj.first_name,
                    "last_name"     : obj.last_name,
                    "email"         : obj.email,
                    "role_name"     : obj.role_id.name,
                    "status"        : obj.status,
                }
            });

            exportUserData = userData.map(obj => {
                return {
                    "username"      : obj.username,
                    "first_name"    : obj.first_name,
                    "last_name"     : obj.last_name,
                    "email"         : obj.email,
                    "role_name"     : obj.role_id.name,
                    "status"        : (obj.status === '1' ? 'Active' : 'Inactive'),
                }
            });

            roleData = userDependantData.map(obj => {
                return {
                    "role_id"   : obj._id,
                    "role_name" : obj.name,
                }
            });

            const paginationData = pagination(currentPage, totalItems, 'user');

            const data = {
                data : {
                    user            : finalData,
                    userData        : exportUserData,
                    role            : roleData,
                },
                ...paginationData
            }
            return handleSuccessResponse(res, data)
        }
        
        throw notFound('No Records Found')

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method to send dependant data for creating record */
exports.AddUser = async (req, res, next) => {
    try {
        let finalData = [];
        const userDependantData = await Role.find({ name: { $ne: 'Administrator' } });
        if(userDependantData){
            finalData = userDependantData.map(obj => {
                return {
                    "role_id" : obj._id,
                    "role_name":obj.name,
                }
            });

            const data = {
                data : {role : finalData}
            }
            return handleSuccessResponse(res, data)
        }
        
        throw notFound('No Records Found')

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method for saving a new record */
exports.saveUser = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors[0].msg)
        }

        const {username, email, first_name, last_name, password, role_id, status} = req.body;
        
        const userExists = await User.findOne({ email: email });
        if (userExists)
            throw validationError('A user with this email already exists.');

        const hashedPw = await bcrypt.hash(password, 12);
        let image = null;
        if(req.file)
            image = req.file.path.replace("\\" ,"/");
        
        var user = new User({
            username,
            first_name,
            last_name,
            email,
            password : hashedPw,
            role_id,
            photo : image,
            status,
            created_by : req.userId //fetched from middleware
        });
        
        const result = await user.save();
        if(result){
            //sendEmail(); //send email

            return handleSuccessResponse(res, { data : 'User Created Successfully' });
        }
        
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
            
        next(err);
    }
}

/**method to fetch a single record */
exports.getUser = async (req, res, next) => {
    try {
        const id = req.params.userId;
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        let finalData, roleData = [];
        const userData = await User.findById(id).populate('role_id');
        if(userData){
            const userDependantData = await Role.find({ name: { $ne: 'Administrator' } });
            finalData = {
                "id"            : userData._id,
                "username"      : userData.username,
                "first_name"    : userData.first_name,
                "last_name"     : userData.last_name,
                "email"         : userData.email,
                "role_id"       : userData.role_id._id,
                "role_name"     : userData.role_id.name,
                "status"        : userData.status,
                'image'		    : userData.photo ? userData.photo : '',
                'imageUrl'		: userData.photo ? imageUrl + userData.photo : ''
            }

            roleData = userDependantData.map(obj => {
                return {
                    "role_id"   : obj._id,
                    "role_name" : obj.name,
                }
            });

            const data = {
                data : {
                    user    : finalData,
                    role    : roleData
                }
            }

            return handleSuccessResponse(res, data);
        }
        
        throw notFound('No Record Found');

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

/**method to update an existing record */
exports.updateUser = async (req, res, next) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw validationError(errors.errors[0].msg)
        }
        
        const userId = req.params.userId;
        if(!isObjectIdValid(userId))
            throw notFound('Record Not Found.');

        const {username, email, first_name, last_name, role_id, status, oldImage} = req.body;
        
        const user = await User.findById(userId);
        if(!user)
            throw notFound('No Records Found');

        let imageUrl = oldImage;
        if(req.file)
            imageUrl = req.file.path.replace("\\","/");

        if(imageUrl != user.photo)
            clearImage(user.photo);
        
        user.username   = username;
        user.email      = email;
        user.first_name = first_name;
        user.last_name  = last_name;
        user.role_id    = role_id;
        user.status     = status;
        user.photo      = imageUrl;
        const result    = await user.save();

        if(result) {
            const data = { data : 'User Updated Successfully' }
            return handleSuccessResponse(res, data);
        }
            
        throw badRequest('Some Error Occured');

    } catch(err){
        if (!err.status)
            err.status = 500;
        
        next(err);
    }
}

/**method to delete record */
exports.deleteUser = async (req, res, next) => {
    try {
        const id = (decodeURIComponent(req.params.userId));
        if(!isObjectIdValid(id))
            throw notFound('Record Not Found.');

        const user = await User.findByIdAndRemove(id);
        if(user){
            const data = {
                data : 'User Deleted Successfully'
            }
            return handleSuccessResponse(res, data);
        }
        

    } catch(err){
        if (!err.status)
            err.status = 500;

        next(err);
    }
}

const clearImage = filePath => {
    filePath = path.join(__dirname, '..', filePath);
    fs.unlink(filePath, err => console.log(err));
}