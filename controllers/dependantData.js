const bcrypt = require('bcryptjs');

const Role = require('../models/role');
const User = require('../models/user');

exports.buildData = async (req, res, next) => {
    try {
        /** add default record in role collection */
        await Role.insertMany([
            {name : 'Administrator'},{name : 'Accounts'},{name : 'Client'}
        ]);
        /** end of adding default record in role collection */

        /** add default record in user collection */
        const role = await Role.findOne({ name: 'Administrator' });
        const hashedPw = await bcrypt.hash('123456', 12);
        var user = new User({
            username: 'Admin',
            first_name : 'William',
            last_name : 'Mates',
            email : 'testing@gmail.com',
            password : hashedPw,
            role_id : role._id
        });

        await user.save();
        /** end of adding default record in user collection */
        
        res.status(200).json({message : 'Data Added Successfully'});
    } catch(err) {
        if (!err.statusCode)
            err.statusCode = 500;
        
        next(err);
    }
}